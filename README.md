# 1on1-report-generator

The 1 on 1 report generator creates a report for use in 1 on 1 meetings with a manager using [Events API](https://docs.gitlab.com/ee/api/events.html#list-currently-authenticated-users-events):

1. Prepare access token for GitLab environment with API scope.
1. Run `ACCESS_TOKEN="<YOUR_TOKEN>" bin/generate-report`.
   - By default generator creates report for `Date.today - 8 days`, it can be overwritten via `--before-date` and `--after-date` options.

The full options for running the tool can be seen by getting the help output by running `bin/generate-report --help`:

```txt
Generate 1on1 Report for User.
Usage: generate-report [options]

Options:
  --environment-url=<s>    The environment's full URL (default: https://gitlab.com)
  -b, --before-date=<s>    Include only events created before a particular date. Default: Date.today. Format: YYYY-MM-DD (default: 2021-12-17)
  -a, --after-date=<s>     Include only events created after a particular date. Default: (Date.today - 8 days). Format: YYYY-MM-DD (default: 2021-12-09)
  -h, --help               Show help message

Environment Variables:
  ACCESS_TOKEN             A valid GitLab Personal Access Token for the specified environment. The token should come from a User that has API permission. (Default: nil)

Examples:
  Generate report for user
  ./bin/generate-report
  Generate report for user events after specific YYYY-MM-DD date
  ./bin/generate-report --after-date 2022-12-25
```

## Output

Output format:

```md
# Report #{opts[:after_date]} - #{opts[:before_date]}

## Merge requests

### Created

#{mr_created_table}

### Reviewed
#{mr_reviewed_table}

## Issue

### Created

#{issues_created_table}

### Closed

#{issues_closed_table}

## Commented / Discussions

#{commented_table} // grouped by unique target (MR, issue) url

```



<details>
<summary>Generate-report logs output example</summary>

```
Checking that GitLab environment 'https://gitlab.com' is available and that provided Access Token works...
Environment and Access Token check was successful - URL: https://gitlab.com, Version: 14.8.0-pre 0437ab6ee5f

Starting to generate report for the user...
[✔] Collecting 10 events
[✔] Collecting 16 events
[✔] Collecting 11 events
[✔] Collecting 6 events
[✔] Collecting 138 events
.
.
.
.
.
.
.
.
.
*
*
*
*
*
*
*
Data generation finished after 17 seconds

█ Report: reports/report-2022-02-17-1645045773.md
```

</details>
